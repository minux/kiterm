/*
 * $Id: ajaxterm.js 7996 2010-12-06 14:58:07Z luigi $
 *
 *  setHTML works around an IE bug that requires content to be installed twice
 * (and still without working handlers)
 */
function setHTML(el, t) { if (!el) return; el.innerHTML = t; el.innerHTML = el.innerHTML; }

ajaxterm={};
ajaxterm.Terminal = function(id, width, height, keylen, sid) {
    if (!width) width=80;
    if (!height) height=25;
    if (!keylen) keylen=16;
    var ie = (window.ActiveXObject) ? 1 : 0;
    var webkit = (navigator.userAgent.indexOf("WebKit") >= 0) ? 1 : 0;
    if (!sid) { /* generate a session ID if not supplied */
	sid = '';
	var alphabet = 'abcdefghijklmnopqrstuvwxyz';
	var l = alphabet.length;
	for (var i=0; i < keylen; i++)
	    sid += alphabet.charAt(Math.round(Math.random()*l));
    }

    /* query0 is the base URL for a query */
    var query0 = "s=" + sid + "&w=" + width + "&h=" + height;
    var query1 = query0 + "&c=1&k=";	/* current query */

    var timeout;	/* callback for the update request */
    var error_timeout;	/* the error callback */
    var keybuf = '';	/* keys to be transmitted */
    var sending = 0;	/* set when we have a pending refresh request */
    var refresh_pending = 0;	/* set when we have a pending refresh request */

    /* elements in the top bar */
    var div = document.getElementById(id);
    var dstat = document.createElement('pre');	/* status line */
    var opt_get = document.createElement('a');
    var opt_color = document.createElement('a');
    var sdebug = document.createElement('span');
    var dterm = document.createElement('div');

    init();
    debug('Session: ' + sid);
    return;

    function debug(s) {	setHTML(sdebug, s); }

    function error() {
	debug("Connection lost at "+((new Date).getTime()));
    }

    function opt_add(opt,name) {
	opt.className = 'off';
	setHTML(opt, ' '+name+' ');
	dstat.appendChild(opt);
	dstat.appendChild(document.createTextNode(' '));
    }

    function do_get(event) { /* toggle get/post */
	opt_get.className = (opt_get.className == 'off') ? 'on' : 'off';
	debug('GET ' + opt_get.className);
    }

    function do_color(event) {
	var o = opt_color.className = (opt_color.className == 'off')?'on':'off';
	query1 = query0 + (o=='on' ? "&c=1" : "") + "&k=";
	debug('Color '+opt_color.className);
    }

    /* we always want a pending update for refresh,
     * plus we want to send one when new data are around.
     */
    function update() {
	
	if (keybuf == '') {	/* outstanding pure refresh ? */
		if (refresh_pending)
			return;
		refresh_pending = 1;
	} else {		/* outstanding data */
		if (sending)
			return;
		sending=1;
	}
	var r=new XMLHttpRequest();
	r.tosend = keybuf;
	keybuf = '';
	var query=query1 + r.tosend;
	if (opt_get.className=='on') {
	    r.open("GET","u?"+query,true);
	    if (ie) { // force a refresh
		r.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
	    }
	} else {
	    r.open("POST","u",true);
	}
	r.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	r.onreadystatechange = function () {
	    if (r.readyState!=4) return;
	    if (error_timeout)
		window.clearTimeout(error_timeout);
	    if (r.status!=200) {
		debug("Connection error, status: "+r.status + ' ' + r.statusText);
		return;
	    }
	    if (r.tosend) {
		sending = 0;
		debug('got ack for ' + r.tosend);
	    } else {
		refresh_pending = 0;
	    }
	    if(ie) {
		var responseXMLdoc = new ActiveXObject("Microsoft.XMLDOM");
		responseXMLdoc.loadXML(r.responseText);
		de = responseXMLdoc.documentElement;
	    } else {
		de=r.responseXML.documentElement;
	    }
	    if (de.tagName=="pre" || de.tagName=="p") {
		setHTML(dterm, unescape(r.responseText));
	    }
	    timeout=window.setTimeout(update, 100); /* next refresh */
	}
	if (error_timeout)
		window.clearTimeout(error_timeout);
	error_timeout=window.setTimeout(error,25000);
	r.send ( (opt_get.className=='on') ? null : query );
    }

    function queue(s) {
	keybuf += s;
	window.clearTimeout(timeout);
	timeout=window.setTimeout(update,10); // 10ms between keys
    }

    function keypress(ev, which) {
	if (!ev) ev = window.event;
	if (!which) which = ev.which;
	else if (which == -1) which = 0;
	s = "kp kC=" + ev.keyCode + " w=" + ev.which + " sh=" +
		ev.shiftKey + " ct=" + ev.ctrlKey + " al=" + ev.altKey;
	debug(s);
	var kc;
	var k = "";
	if (ev.keyCode)
	    kc = ev.keyCode;
	if (which)
	    kc = which;
	if (0 && ev.altKey) { /* ALT-char ==> ESC + char */
	    if (kc >= 65 && kc <= 90)
		kc += 32;
	    if (kc >= 97 && kc <= 122)
		k = String.fromCharCode(27) + String.fromCharCode(kc);
	} else if (ev.ctrlKey || ev.altKey) {
	    if (kc >= 65 && kc <= 90)
		k = String.fromCharCode(kc - 64); // Ctrl-A..Z
	    else if (kc >= 97 && kc <= 122)
		k = String.fromCharCode(kc - 96); // Ctrl-A..Z
	    /* XXX check below, kc values do not match */
	    else if (kc ==  54)  k=String.fromCharCode(30); // Ctrl-^
	    else if (kc == 109) k=String.fromCharCode(31); // Ctrl-_
	    else if (kc == 219) k=String.fromCharCode(27); // Ctrl-[
	    else if (kc == 220) k=String.fromCharCode(28); // Ctrl-\
	    else if (kc == 221) k=String.fromCharCode(29); // Ctrl-]
	    else if (kc == 219) k=String.fromCharCode(29); // Ctrl-]
	    else if (kc == 219) k=String.fromCharCode(0);  // Ctrl-@
	} else if (which==0) {
	    if (kc==9) k=String.fromCharCode(9);  // Tab
	    else if (kc==8) k=String.fromCharCode(127);  // Backspace
	    else if (kc==27) k=String.fromCharCode(27); // Escape
	    else {
		if (kc==33) k="[5~";        // PgUp
		else if (kc==34) k="[6~";   // PgDn
		else if (kc==35) k="[4~";   // End
		else if (kc==36) k="[1~";   // Home
		else if (kc==37) k="[D";    // Left
		else if (kc==38) k="[A";    // Up
		else if (kc==39) k="[C";    // Right
		else if (kc==40) k="[B";    // Down
		else if (kc==45) k="[2~";   // Ins
		else if (kc==46) k="[3~";   // Del
		else if (kc==112) k="[[A";  // F1
		else if (kc==113) k="[[B";  // F2
		else if (kc==114) k="[[C";  // F3
		else if (kc==115) k="[[D";  // F4
		else if (kc==116) k="[[E";  // F5
		else if (kc==117) k="[17~"; // F6
		else if (kc==118) k="[18~"; // F7
		else if (kc==119) k="[19~"; // F8
		else if (kc==120) k="[20~"; // F9
		else if (kc==121) k="[21~"; // F10
		else if (kc==122) k="[23~"; // F11
		else if (kc==123) k="[24~"; // F12
		if (k.length)
		    k=String.fromCharCode(27)+k;
	    }
	} else {
	    if (kc==8)
		k=String.fromCharCode(127);  // Backspace
	    else
		k=String.fromCharCode(kc);
	}
	if (k.length)
	    queue( encodeURIComponent(k) );
	/* javascript tricks to block propagation */
	ev.cancelBubble=true;
	if (ev.stopPropagation) ev.stopPropagation();
	if (ev.preventDefault)  ev.preventDefault();
	return false;
    }

    function keydown(ev) {
	if (!ie && !webkit) return;
	/* webkit and IE do not pass control key to keypress, so we
	 * remap certain keys. Note that we cannot set ev.which=0
	 * because it is passed by reference and is not writable.
	 */
	if (!ev) ev = window.event;
	s="kd kC="+ev.keyCode+" w="+ev.which+" sh=" +
		    ev.shiftKey+" ct="+ev.ctrlKey+" al="+ev.altKey;
	debug(s);
	/* keycodes to be mapped into keypress */
	o={9:1,8:1,27:1,33:1,34:1,35:1,36:1,37:1,38:1,39:1,40:1,45:1,46:1,112:1,
		113:1,114:1,115:1,116:1,117:1,118:1,119:1,120:1,121:1,122:1,123:1};
	if (o[ev.keyCode] || ev.ctrlKey || ev.altKey)
	    return keypress(ev, -1 /*ignore ev.which */);
    }

    function keyup(ev) { /* used to trap ESC and kindle keys */
	if (!ie && !webkit) return;
	if (!ev) ev = window.event;
	s = "ku kC="+ev.keyCode+" w="+ev.which+" sh=" +
		    ev.shiftKey+" ct="+ev.ctrlKey+" al="+ev.altKey;
	debug(s);
	/* kindle-specific, map left BACK to escape */
	if (ev.keyCode == 33) queue("%1B");
	return false;
    }

    function init() {
	opt_add(opt_color,'Colors');
	opt_color.className = 'on';
	opt_color.title = 'Toggle color or grey';
	opt_add(opt_get,'GET');
	opt_get.title = 'Toggle GET or POST methods';
	dstat.appendChild(sdebug);
	dstat.className = 'stat';
	div.appendChild(dstat);
	div.appendChild(dterm);
	if (opt_color.addEventListener) {
	    opt_get.addEventListener('click',do_get,true);
	    opt_color.addEventListener('click',do_color,true);
	} else {
	    opt_get.attachEvent("onclick", do_get);
	    opt_color.attachEvent("onclick", do_color);
	}
	document.onkeypress = keypress;
	document.onkeydown = keydown;
	document.onkeyup = keyup;
	timeout=window.setTimeout(update,100);
    }
}
