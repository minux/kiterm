/*
 * Copyright (C) 2010 Luigi Rizzo, Universita' di Pisa
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * $Id: http.c 7996 2010-12-06 14:58:07Z luigi $

Backend for web-based terminal.
This implements a very simple web server,
and an application specific part in charge of
passing shell data over http.

http routines

 */

#include "dynstring.h"
#include "myts.h"
#include "terminal.h"
#include <netdb.h>      /* gethostbyname */

#include <ctype.h>
#include <sys/stat.h>
#include <sys/mman.h>   /* PROT_READ and mmap */
#ifdef linux
#include <string.h>     /* strcasestr */
/* strcasestr prototype is problematic */
char *strcasestr(const char *haystack, const char *pneedle);
#endif

extern char *map437[256];
int build_map437(int argc, char *argv[]);

#define INBUFSZ	4096
/*
 * struct client contains support for talking to the browser.
 * It contains a buffer for receiving the incoming request,
 * hold a copy of the response, and support for an mmapped file.
 * Initially, reply = 0, len = sizeof(inbuf) and pos = 0,
 * we accumulate data into inbuf and call parse_msg() to tell
 * whether we are done.
 * During the reply phase, inbuf contains the response hdr,
 * possibly map contains the body, len = header length, body_len = body_len
 * and pos = 0. We first send the header, then toggle len = -body_len,
 * and then send the body. When done, detach the buffer.
 * If filep is set, then map is a mapped file, otherwise points to
 * some other buffer and does not need to be freed.
 */
struct client {
	struct sess	sess;
	int		reply;		/* set when replying */
	int		pos, len;		/* read position and length */
	struct timeval	due;	/* when a response is due */
	struct sess	*sh;
	char		inbuf[INBUFSZ];	/* I/O buffer */
	dynstr		response;

	/* memory mapped file */
	int		filep;
	int		body_len;
	char		*map;
};

/* supported mime types -- suffix space mime. Default is text/plain.
 * processed by getmime(filename)
 */
static const char *mime_types[] = {
	"html text/html",
	"htm text/html",
	"js text/javascript",
	"css text/css",
	NULL
};

struct http_desc {
	struct sockaddr_in sa;
	int		unsafe;	/* allow unsafe file access */
	char		*cmd;	/* command to run */
	int		http_timeout;	/* timeout for http session */
};

struct http_desc desc, *http;	/* set to me.app->data */

static void add_digit(int *c, char ** const src)
{
	const char *hex = "0123456789abcdef0123456789ABCDEF";
	const char *p = *src;
	p = (*p) ? index(hex, *p) : NULL;
	if (!p)
		return;
	*c = *c * 16 + ((p - hex)&0xf);
	(*src)++;
}

/* convert the html encoding back to plain ascii
 * XXX must be fixed to handle UTF8
 */
static char *unescape(char *s)
{
	char *src, *dst;
	int c;

	for (src = dst = s; *src; ) {
		c = *src++;
		if (c == '+')
			c = ' ';
		else if (c == '%') {
			c = '\0';
			add_digit(&c, &src);
			add_digit(&c, &src);
		}
		*dst++ = c;
	}
	*dst++ = '\0';
	return s;
}

static int build_response(struct client *ss, const char *ret);

/* process requests coming from the browser */
int u_mode(struct client *ss, char *body)
{
    /* request parameters */
    char *s = NULL, *k = "";
    int refresh = 0, rows = 0, cols = 0, color=0;
    char *cur, *p, *p2;
    int i;
    struct sess *sess;

    /* extract parameters */
    for (p = body; (cur = strsep(&p, "&")); ) {
	if (!*cur) continue;
	p2 = strsep(&cur, "=");
	if (!strcmp(p2, "s")) s = cur;	// session id
	if (!strcmp(p2, "w")) cols = atoi(cur);	// width
	if (!strcmp(p2, "h")) rows = atoi(cur);	// height
	if (!strcmp(p2, "c")) color = atoi(cur);	// color
	if (!strcmp(p2, "k")) k = cur;	// keys
	if (!strcmp(p2, "r")) refresh = 1; /* force refresh */
    }
    if (!s || !*s) { /* list sessions */
        dsprintf(&ss->response,
            "HTTP/1.1 200 OK\r\n"
            "Content-Type: text/html\r\n\r\n"
	    "<p><h1>Active sessions</h1>\n");
	/* XXX explore current sessions ... */
	i = 0;
	for (sess = __me.sess; sess; sess = sess->next) {
	    const char *name = term_name(sess);
	    if (name)
		dsprintf(&ss->response,
		    "%2d <a href=\'/?s=%s\'>%s</a>\n", ++i, name, name);
	}
	dsprintf(&ss->response, "</p>\n");
	ss->len = ds_len(ss->response);
	return 0;
    }

    sess = term_find(s);
    if (!sess)
        sess = term_new(http->cmd, s, rows, cols, NULL);
    if (!sess) {
	dsprintf(&ss->response,
		"HTTP/1.1 400 fork failed\r\n\r\n");
	ss->len = ds_len(ss->response);
	return 0;
    }
    if (k[0]) {
	unescape(k);
	term_keyin(sess, k);
    }

    ss->sh = sess; /* prepare for updates */
    gettimeofday(&ss->due, NULL);
    if (!k[0] && !refresh && !term_state(sess, NULL)) {
	DBG(2, "/* no modifications, wait before reply */\n");
	ss->due.tv_sec += http->http_timeout;
    }
    return 0;
}

static int build_response(struct client *ss, const char *ret)
{
    struct term_state st = { .flags = TS_MOD, .modified = 0 };
    int mod = term_state(ss->sh, &st);

    if (!ret) { /* not supplied by the user */
	ret = (mod) ?
		"<pre class=\"term kindle\">" : "<idem></idem>";
    }
    dsprintf(&ss->response,
            "HTTP/1.1 200 OK\r\n"
            "Content-Type: text/xml; charset=cp437;\r\n\r\n"
	    "<?xml version=\"1.0\" ?>%s", ret);
    if (ss->sh) {
	const char *src = st.data;
	unsigned char done = '\0';
	int i;

	/* convert text so that it does not interfere with the XML.
	 * mostly, we need utf8 encoding for 'special' characters
	 * Colors and the like are dealt with using '<span class...>'
	 * (surely needed for the cursor).
	 * Make sure we always have rows*cols chars.
	 */
	for (i=0; i < st.rows * st.cols;) {
	    unsigned char cc = done ? done : (unsigned char)src[i];
	    int use_span = i == st.cur;
	    if (!cc) done = cc = ' ';
	    /* XXX todo: if there is a color change, close previous span
	     * and open a new one if needed.
	     */
	    if (use_span)
		dsprintf(&ss->response, "<span class=\"b1\">");
	    /* some chars are unmodified */
#if 1
	    if (map437[cc])
		dsprintf(&ss->response, "%s", map437[cc]); /* entity chars */
	    else if (isalnum(cc) || index("\n ", cc)) /* XXX maybe more */
		dsprintf(&ss->response, "%c", cc); /* unmodified chars */
	    else
		dsprintf(&ss->response, "%%%02x", cc); /* escaped chars */
#else
	    if (isalnum(cc) || index("\n ", cc)) /* XXX maybe more */
		dsprintf(&ss->response, "%c", cc); /* unmodified chars */
	    else if (cc <= 0x7f)	/* one-byte utf8 */
		dsprintf(&ss->response, "%%%02x", cc);
	    else			/* two-byte codes */
		dsprintf(&ss->response, "%%%02x%%%02x",
			0xc0+(cc>>6), 0xc0+(cc&0x3f));
#endif
	    /* XXX and here i should do three-byte codes */
	    /* XXX the span should not close here */
	    if (use_span)
		dsprintf(&ss->response, "</span>");
	    if (++i % st.cols == 0)
		dsprintf(&ss->response, "\n"); /* unmodified chars */
	}
	DBG(3, "done %d chars at %p %s\n", i, src, ds_data(ss->response));
	/* XXX also close the main span */
	dsprintf(&ss->response, "</pre>");
    }
    ss->len = ds_len(ss->response);
    DBG(2, "response %s\n", ds_data(ss->response));
    return 0;
}

struct client *new_client(int fd, cb_fn cb, void *arg)
{
    struct client *s = new_sess(sizeof(struct client), fd, cb, arg);

    if (!s)
	return NULL;
    s->filep = -1;	/* no file */
    s->pos = 0;
    s->len = sizeof(s->inbuf);
    return s;
}

static int parse_msg(struct client *s);
static int http_write(struct client *s);
/*
 * callback for the http session
 */
int handle_http(void *_s, struct cb_args *a)
{
	struct client *s = _s;
	if (a->run == 0) {
		if (s->reply)
			timersetmin(&a->due, &s->due);
		if (!s->reply)
			FD_SET(s->sess.fd, a->r);
		else if ( term_state(s->sh, NULL) ||
				timercmp(&s->due, &a->now, <))
			FD_SET(s->sess.fd, a->w);
		else
			return 0;
		return 1;
	}
	if (FD_ISSET(s->sess.fd, a->r)) {
		int l;
		/* accumulate request */
		l = read(s->sess.fd, s->inbuf + s->pos, s->len - s->pos);
		DBG(2, "read %p returns %d %s\n", s, l, s->inbuf);
		if (l <= 0) {
			DBG(1, "buf [%s]\n", s->inbuf);
			s->len = 0; // mark done with read
		} else {
			s->pos += l;
		}
		parse_msg(s); /* check if msg is complete */
	}
	if (FD_ISSET(s->sess.fd, a->w)) {
		http_write(s);
	}
	if (s->len != 0) /* socket still active */
		return 0;
	/* else dead */
	ds_free(s->response);
	free(s);
	return 1;
}

/* support function to return mime types. */
static const char *getmime(const char *res)
{
    const char **p, *suffix;
    int lres = strlen(res);

    res += lres;	/* move to the end of the resource */
    for (p = mime_types; (suffix = *p); p++) {
	int lsuff = strcspn(suffix, " ");
	if (lsuff > lres)
	    continue;
	if (!bcmp(res - lsuff, suffix, lsuff))
	    return suffix + lsuff + 1;
    }
    return "text/plain";	/* default */
}

/*
 * A stripped down parser for http, which also interprets what
 * we need to do.
 */
static int parse_msg(struct client *s)
{
    char *a, *b, *c = s->inbuf;	/* strsep support */
    char *body, *method = "", *resource = "";
    int row, tok;
    int clen = -1;
    char *err = "generic error";

    if (s->pos == s->len) { // buffer full, we are done
	DBG(0, "--- XXX input buffer full\n");
	s->len = 0; // complete
    }
    /* locate the end of the header. If not found, just return */
    body = strstr(s->inbuf, "\n\r\n") + 3;
    if (body < s->inbuf)
	body = strstr(s->inbuf, "\n\n") + 2;
    if (body < s->inbuf && s->len)
	return 0;
    /* quick search for content length */
    a = strcasestr(s->inbuf, "Content-length:");
    if (a && a < body) {
	sscanf(a + strlen("Content-length:") + 1, "%d", &clen);
	DBG(2, "content length = %d, body len %d\n",
		clen, s->pos - (body - s->inbuf));
	if (s->pos - (body - s->inbuf) != clen)
	    return 0; /* body incomplete */
    }
    /* if no content length, hope body is complete */
    /* now parse the header */
    for (row=0; (b = strsep(&c, "\r\n"));) {
	if (*b == '\0')
	    continue;
	if (b > body) {
	    body = b;
	    break;
	}
	row++;
        for (tok=0; (a = strsep(&b, " \t"));) {
	    if (*a == '\0')
		continue;
	    tok++;
	    if (row == 1) {
		if (tok == 1)
		    method = a;
		if (tok == 2)
		    resource = a;
	    }
	}
    }
    s->reply = 1; /* body found, we move to reply mode. */
    DBG(2, "%s %s\n+++ request body [%s]\n", method, resource, body);
    s->pos = 0;
    if (!strcmp(method, "POST") && !strcmp(resource, "/u")) {
	return u_mode(s, body);	/* ajax request using POST */
    } else if (!strcmp(method, "GET") && !strncmp(resource, "/u?", 3)) {
	return u_mode(s, resource+3); /* ajax request using GET */
    } else {	/* request for a file, map and serve it */
	struct stat sb;
	char *query_args = resource;

	resource = strsep(&query_args, "?&");
	err = "invalid pathname";
	if (!http->unsafe && resource[1] == '/')
	    goto error;	/* avoid absolute pathnames */
	for (a = resource+1; *a; a++) { /* avoid back pointers */
	    if (*a == '.' && a[1] == '.')
		goto error;
	}
	if (!strcmp(resource, "/"))
	    resource = "/ajaxterm.html";
	s->filep = open(resource+1, O_RDONLY);
	err = "open failed";
	if (s->filep < 0 || fstat(s->filep, &sb))
	    goto error;
	err = "mmap failed";
	/* linux wants MAP_PRIVATE or MAP_SHARED, not 0 */
	s->map = mmap(NULL, (int)sb.st_size, PROT_READ, MAP_PRIVATE, s->filep, (off_t)0);
	if (s->map == MAP_FAILED)
	    goto error;
	s->body_len = sb.st_size;
        dsprintf(&s->response,
	    "HTTP/1.1 200 OK\r\n"
	    "Content-Type: %s\r\n"
	    "Content-Length: %d\r\n\r\n",
		getmime(resource+1),
		(int)sb.st_size);
	s->len = ds_len(s->response);
	return 0;
    }
error:
    if (s->filep >= 0)
	close(s->filep);
    dsprintf(&s->response,
	"HTTP/1.1 200 OK\r\n"
	"Content-Type: text/plain\r\n\r\nResource %s : %s\n", resource, err);
    s->len = ds_len(s->response);
    return 0;
}

static int http_write(struct client *s)
{
	int l;

	if (s->response == NULL)
		build_response(s, NULL);
	/* first write the header, then set s->len negative and
	* write the mapped file
	*/
	if (s->len > 0) {
		l = write(s->sess.fd, ds_data(s->response) + s->pos, s->len - s->pos);
	} else {
		l = write(s->sess.fd, s->map + s->pos, s->body_len - s->pos);
	}
	if (l <= 0)
		goto write_done;
	s->pos += l;
	DBG(1, "written1 %d/%d\n", s->pos, s->len);
	if (s->pos == s->len) { /* header sent, move to the body */
		s->len = -s->body_len;
		s->pos = 0;
	}
	DBG(1, "written2 %d/%d\n", s->pos, -s->len);
	if (s->pos == -s->len) { /* body sent, close */
write_done:
		DBG(1, "reply complete\n");
		/* the kindle wants shutdown before close */
		shutdown(s->sess.fd, SHUT_RDWR);
		close(s->sess.fd);
		s->len = 0;
		if (s->filep) {
			if (s->map)
				munmap(s->map, s->body_len);
			close(s->filep);
		}
	}
	return 0;
}

/* callback for the listener socket */
int handle_listen(void *_s, struct cb_args *a)
{
    struct sockaddr_in sa;
    unsigned int l = sizeof(sa);
    int fd;
    struct client *s = _s;

    if (a->run == 0) { /* always listen */
	FD_SET(s->sess.fd, a->r);
	return 1;
    }

    if (!FD_ISSET(s->sess.fd, a->r))
	return 0;

    DBG(1, "listening socket\n");
    bzero(&sa, sizeof(sa));
    fd = accept(s->sess.fd, (struct sockaddr *)&sa, &l);
    if (fd < 0) {
	DBG(0, "listen failed\n");
	return -1;
    }
    new_client(fd, handle_http, NULL);
    return 0;
}

/* init routine, fill default parameters */
static int http_init(void)
{
	struct sockaddr_in *sa;

	http = __me.app->data;
	http->http_timeout = 20;
	sa = &http->sa;

	DBG(1, "http init routine\n");
	memset(sa, 0, sizeof(*sa));
	sa->sin_family = PF_INET;
	sa->sin_port = htons(8022);
	inet_aton("127.0.0.1", &sa->sin_addr);
	http->cmd = "login";
	build_map437(0, NULL);
	return 0;
}

/*
 * fetch arguments from the command line
 */
static int http_parse(int *ac, char *argv[])
{
    int argc = *ac;

    DBG(1, "http parse routine\n");
    for ( ; argc > 1 ; argc--, argv++) {
        char *optval, *opt = argv[1];
        /* options without arguments */
        if (!strcmp(opt, "--unsafe")) {
            http->unsafe = 1;
            continue;
        }
        if (argc < 3)
            break;
        /* options with argument */
        optval = argv[2];
        argc--;
        argv++;
        if (!strcmp(opt, "--cmd")) {
            http->cmd = optval;
            continue;
        }
        if (!strcmp(opt, "--port")) {
            http->sa.sin_port = htons(atoi(optval));
            continue;
        }
        if (!strcmp(opt, "--addr")) {
            struct hostent *h = gethostbyname(optval);
            if (h) {
                http->sa.sin_addr = *(struct in_addr *)(h->h_addr);
            } else if (!inet_aton(argv[1], &http->sa.sin_addr)) {
                perror("cannot parse address");
                return 1;
            }
            continue;
        }
        break;
    }
    return 0;
}

/* create the session */
static int http_start(void)
{
	struct sockaddr_in *sa = &http->sa;

	DBG(1, "listen on %s:%d cmd %s\n",
		inet_ntoa(sa->sin_addr), ntohs(sa->sin_port), http->cmd);
	new_client(opensock(*sa, 0 /* TCP */, 0 /* server */),
		handle_listen, NULL);
	return 0;
}

struct app http_app = { .init = http_init,
	.parse = http_parse, .start = http_start, .data = &desc};
