#! /bin/sh
# /etc/init.d/kiterm
case "$1" in
  start)
    echo "Starting kiterm "
    /mnt/us/usbnet/kiterm/myts.arm &
    ;;
  stop)
    echo "Stopping kiterm "
    killall myts.arm
    ;;
  *)
    echo "Usage: /etc/init.d/kiterm {start|stop}"
    exit 1
    ;;
esac
exit 0
